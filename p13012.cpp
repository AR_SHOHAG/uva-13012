#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
    int t, arr[5], i, ck=0;
    while(scanf("%d", &t) !=EOF){
        for(i=0;i<5;++i){
            scanf("%d", &arr[i]);
            if(t==arr[i])
                ck++;
        }
        printf("%d\n", ck);
        ck=0;

    }
    return 0;
}
